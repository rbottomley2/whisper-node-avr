# Overview #
This repository contains a set of code, samples, libraries, scripts and documentation for the Wisen Talk² Whisper Node - AVR, an ultra-low power version of the Arduino with built-in Wireless communication and capable of running for years on a single AA battery.

For additional information about this product or about the Talk² project, please visit:

* Wisen website and on-line store at http://wisen.com.au
* Our official discussion forum: http://talk2forum.wisen.com.au

![Whisper Node board, antenna, and AA battery](https://bitbucket.org/repo/drxzG7/images/1440280951-board_antenna_mini.jpg)

|                 |                                        |
|----------------:|:---------------------------------------|
|**Product:**     |Talk² Whisper Node - AVR                |
|**Version:**     |1.0v - Official Release                 |
|**Release Date:**|10 September 2017                       |
|**Status:**      |Shipping                                |

*Use GIT Tags to navigate between different versions/releases.*

## Special Notes ##
Important notes regarding the Whisper Node:

* **This is a 3.3 V board** — Although the MCU (Atmega328p) can run at 5 V, there are components, like the external SPI flash and the RFM69 radio module, that will not tolerate 5 V. If you need to connect a 5 V device to any of the pins, make sure that the pin is shared with neither the external flash nor the RFM69.
* **FTDI Voltage** — When powering the board via the FTDI pins, make sure the supplied voltage is between 3.4 V and 6 V as this will be using the LDO regulator (same as VIN).
* **Quick start** — If you are looking how to set up your Arduino IDE to work with this board, jump straight into the "Software" topic on this page to learn how to install the Whisper Node Board, Talk² Library and other required libraries to run the examples.

## Table of Contents ##

[TOC]

# Hardware #

## Specifications ##

### Dimensions and Connections ###
* Board size: 65.65 mm x 26.75 mm x 1.6 mm + overhanging connectors
* GPIOs: Two 17-pin PCB headers to access all of the MCU pins, VIN, VBAT, and both of the 3.3 V rails
* Standard 3x2 ISP header
* 6-pin FTDI header
* Micro-USB for external power
* PicoBlade 1.25 mm for battery connection
* RF SMA connection for external antenna
 
### Components ###
* MCU Atmel 8-bit AVR: ATMega328P-AU (32 KB Flash/2 KB RAM)
* 4 Mbit SPI flash: W25X40
* Sub-GHz RF module: RFM69
* Step-up switching regulator: MCP16251
* Dual LDO regulator: AP7332
* 16 Mhz high precision crystal
* 2x feedback LEDs (blue and yellow)
* 2x tactile user buttons
* 2 dB omni-directional SMA antenna
 
### Optional Upgrade Kits ###
* CR2032 battery holder
* Real-time clock kit: Maxim DS3231M
* High input voltage LDO 1117 kit

## Board Layout ##
The layout below shows the position for the most important connectors and components.

```
#!
             ┌─────  26.65mm  ─────┐

                   ╓───╖
                  ┌╢   ╟┐     26.65x64.65mm
             ╔════╡ CN3 ╞══════════╗        ┐
             ║┌─┐ └─────┘┌┐┌┐(H)┌─┐║        │ <--- Hole 2
             ║│■│     LD1└┘└┘LD2│■│║        │   20.0x61.5mm
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│<--P1     P2-->│o│║        │
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│  ┌───┐ ┌───┐  │o│║        │
             ║│o│  │BT1│ │BT2│  │o│║        │
             ║│o│  └───┘ └───┘  │o│║
             ║│o│               │o│║      64.65mm
             ║│o│               │o│║
             ║│o├───┐<--P3:ISP  │■│║        │
             ║│o│o o│         ┌─┤o│║        │
             ║│■│o o│P6:RTC-->│o│o│║        │
             ║│o├─┬─┴─────────┤o│o│║        │
             ║│o│ │o o o o o ■│o│o│║        │
             ║└─┘ └───────────┴─┴─┘║        │         Mounting Hole Diameter: 2.75mm
            ┌╨──╖     P4:FTDI      ║        │         Recommended Screw: Nylon M2.5
            │CN1║                  ║        │
            └╥──╜                  ║        │
Hole 1 -->  ┌╨╖  (H)           ┌───╢        │
7.8x10.3mm  └╥╜CN2    P5:VCC-->│o ■║        │
             ╚═════════════════╧═══╝        ┘
           0x0mm
```

### Pinouts ###
The Talk² Whisper Node has many headers and connectors. All pins are positioned on a 2.54 grid and all PCB headers are shipped with the board, making it breadboard compatible.

Pinout details are as follows:

#### Extension Headers — P1 and P2 ####
The extension headers provide direct access to the MCU pins. Pins already in use by the Whisper Node board will have their function displayed as [BOARD_USED]. Board functions displayed as (BOARD_OPTIONAL) are optional and not connected by default.

Note that some of the board functions can be disabled by cutting PCB traces or removing components. (See the appropriate section for more information.)
```
#!
                                                  P1            P2
                                                   ┌─┐           ┌─┐
                                           GND ────│■│           │■│──── GND
            [RFM69_INT]         INT0:32:PD2:D2 ────│o│           │o│──── AREF:20
              (RTC_INT)      INT1/PWM:1:PD3:D3 ────│o│           │o│──── D1:PD1:31:TXD
                 [BTN1]               2:PD4:D4 ────│o│           │o│──── D0:PD0:30:RXD
                 [BTN2]           PWM:9:PD5:D5 ────│o│           │o│──── A7:ADC7:22                [VIN_Voltage]
                 [LED1]          PWM:10:PD6:D6 ────│o│           │o│──── A6:ADC6:19                [VBAT_Voltage]
            [RFM69_RST]              11:PD7:D7 ────│o│           │o│──── A5/D19:PC5:28:ADC5/SCL
            [W25X40_CS]              12:PB0:D8 ────│o│           │o│──── A4/D18:PC4:27:ADC4/SDA
                 [LED2]          PWM:13:PB1:D9 ────│o│           │o│──── A3/D17:PC3:26:ADC3
             [RFM69_CS]      SS/PWM:14:PB2:D10 ────│o│           │o│──── A2/D16:PC2:25:ADC2
         [RFM69/W25X40]    MOSI/PWM:15:PB3:D11 ────│o│           │o│──── A1/D15:PC1:24:ADC1        (RTC_VIN)
         [RFM69/W25X40]        MISO:16:PB4:D12 ────│o│           │o│──── A0/D14:PC0:23:ADC0        [VBAT_Voltage_MOSFET]
         [RFM69/W25X40]         SCK:17:PB5:D13 ────│o│           │■│──── GND
                                    29:PC6:RST ────│o│           │o│──── 3V3_R1
                                           GND ────│■│           │o│──── 3V3_R1
                                           VIN ────│o│           │o│──── 3V3_R2
                                          VBAT ────│o│           │o│──── 3V3_R2
                                                   └─┘           └─┘

```
#### Antenna — CN3 ####
The 50 Ω SMA female connector is attached to the RFM69 radio module. To prevent any damage to the RF module, make sure there is always an antenna connected when transmitting.

To improve the range, a bigger or directional antenna may be used. Remember to keep the cable as short as possible to minimize signal loss.

Whisper Node is normally shipped with a 2 dB whip antenna.

#### Micro-USB — CN1 and PicoBlade — CN2 ####
The Talk² Whisper Node can be powered by battery (VBAT) or an external power supply (VIN). The Micro-USB connector is the standard way to connect a PSU to the board and the supplied voltage must be between 3.7 V and 6 V, a mobile phone USB AC charger or a USB power bank work well.

For the battery connection, a power source between 0.9 V and 3.3 V can be attached to the Molex PicoBlade connector. A 2xAA battery holder is the perfect example.

More details about the powering options can be found further on in this document.
```
#!
          ║
          ║
         ┌╨───────╖*
         │       ─╢──── 1:VIN
         │       ─╢──── 2:NC
         │  CN1  ─╢──── 3:NC
         │       ─╢──── 4:NC
         │       ─╢──── 5:GND
         └╥───────╜
          ║
         ┌╨────╖*
         │    ─╢──── 1:VBAT
         │ CN2 ║
         │    ─╢──── 2:GND
         └╥────╜
          ╚═══════════

```

#### Programming Interfaces ISP — P3 and FTDI — P4 ####
These are the headers to access ISP and FTDI connections to the MCU. It is important to note that although the MCU is 5 V compatible, the RFM69 radio module and the SPI flash memory chip are not. For this reason, never connect a 5 V ISP programmer to the board or it might damage those components.
```
#!
           P3 - ICSP                   P4 - FTDI
     ┌───────────────────┐       ┌─────────────────────┐

          ┌──── 5:RESET             ┌─────────────┐*
          │ ┌── 3:SCK               │ o o o o o ■ │
          │ │ ┌ 1:MISO              └─────────────┘
          │ │ │                       │ │ │ │ │ │
        ┌───────┐*                    │ │ │ │ │ └ 1:GND
        │ o o o │                     │ │ │ │ └── 2:NC
        │ ■ o o │                     │ │ │ └──── 3:VIN
        └───────┘                     │ │ └────── 4:RXD
          │ │ │                       │ └──────── 5:TXD
          │ │ └ 2:VIN                 └────────── 6:RTS
          │ └── 4:MOSI
          └──── 6:GND

```
### Buttons and LEDs ###
The Talk² Whisper Node buttons are not simple tactile switches connected to the MCU Pins; they actually include a small debounce circuit. If you are not familiar with the concept, a debounce circuit prevents a switch oscillating between states at high frequency when pressed, smoothing its operation.

![Whisper Node button debounce circuit diagram](https://bitbucket.org/repo/drxzG7/images/1524179939-WhisperNode_BT_Debounce_Circuit.png)

| Designator | Normal | Active | MCU Pin | Low to High | High to Low |
|------------|--------|--------|---------|-------------|-------------|
| BT1        | LOW    | HIGH   | D4      | 0.5 ms      | 8.0 ms      |
| BT2        | LOW    | HIGH   | D5      | 0.5 ms      | 8.0 ms      |

*Note that both BT1 and BT2 are normal LOW, active HIGH. This has been designed like this to minimize any current leakage but mainly to be a bit more human-friendly as an active LOW switch might be confusing.*

![Whisper Node button debounce low-to-high voltage trace](https://bitbucket.org/repo/drxzG7/images/2067308204-WhisperNode_BT_Debounce_Low-High.png)
![Whisper Node button debounce high-to-low voltage trace](https://bitbucket.org/repo/drxzG7/images/2877258136-WhisperNode_BT_Debounce_High-Low.png)

For reference, there is an interesting debounce online calculator at http://protological.com/debounce-calaculator/ which can assist you in calculatng the resistor and capacitor values when designing such circuits.

The board has two simple 0603 LEDs with resistors at the anode side, directly connected to an MCU pin:

| Designator | Color   | Resistor | MCU Pin |
|------------|---------|----------|---------|
| LD1        | Blue    | 220R     | D6      |
| LD2        | Yellow  | 220R     | D9      |

*As a good-practice suggestion to preserve power, never light an LED and perform a radio transmission at the same time. This will help to keep the maximum board consumption current as low as possible. Also, a 10 ms to 25 ms blink is normally more than enough to be noticed by most humans and will keep the energy consumption low.*

### Add-Ons RTC, LDO and Coin Battery ###
To make use of all PCB real estate, the Talk² Whisper Node has a few blank Pads. With the correct components, the board functionalities can be easily extended to add the following features:

* I²C real-timecClock with built-in crystal and temperature sensor
* Extra 1117 LDO regulator for input voltages over 12 V
* CR2032 coin cell battery holder (cannot be used together with 1117 LDO)

#### LDO — P5 ####
Even with Talk² Whisper Node being designed to run on batteries, it can also be powered by an external PSU limited to 5.5 V. In case the supplied voltage is greater than 5.5 V, a standard 1117 LDO regulator can be soldered to the back of the board together with input and output 0805 ceramic capacitors.

The 1117 LDOs are not the most efficient regulators, with higher quiescent current and significant drop-out. The good thing about these devices is that the maximum input voltage is normally between 15 V and 21 V, depending on the manufacturer/model, making them a great option when powering from a 12 V source, for example.

The 1117 LDO output is connected directly to VIN, before the input diode, the same way an external USB power supply would be connected. For this reason, independent of the model used, it must output 5.5 V and supply a minimum of 700 mA.

**Attention:*** do not power the board through an 1117 LDO and an external power supply connected to the Micro-USB at same time! It is OK to power the board via the 1117 LDO and have a battery connected to VBAT working as backup power.

Suggested parts:

* Diodes AZ1117IH-5.0TRG1 - http://www.diodes.com/catalog/Single_LDOs_50/AZ1117_10276

##### Assemble #####

* LDO 1117 - SOT223: **U5**
* Ceramic Capacitor 10 uF 35 V - SMD 0805: **C19 and C20**
* PCB Block - 2.54 mm: **P5**

#### RTC — P6 ####
The Talk² Whisper Node has a SOIC-8 footprint on the back to add an I²C real-time clock with built-in crystal. The footprint matches the Maxim DS3231MZ+ (https://datasheets.maximintegrated.com/en/ds/DS3231M.pdf) and the Maxim DS3232MZ+ (https://datasheets.maximintegrated.com/en/ds/DS3232M.pdf) is also compatible.

If necessary, special ports from the RTC chip can be accessed via P6 described below:

```
#!
       ┌─┐
       │o│──── RST
       │o│──── INT/SQW
       │o│──── 32kHz
       └─┘
```

The RTC chip has two power inputs, VCC and VBAT. Normally when running by VCC, the RTC consumes more power but it also has all features turned on. On the other hand, when powered exclusively by VBAT, the chip will run in time-keeping mode only to save energy but still responds to I²C communications.

By default, the RTC VCC pin is connected to the 3V3_R2 and the VBAT to the 3V3_R1 on the Talk² Whisper Node. When the board is powered only by battery, the VCC will be off, forcing the Clock to run in low-power mode.

The board also makes it possible to connect the VCC to the MCU pin A1 using a solder jumper, so that the RTC VCC can be powered by turning the A1 Pin HIGH/LOW via software. Additionally, the RTC INT can be connected to the MCU D3/INT1 pin via a solder jumper, so the RTC Alarms can be used to trigger interrupts on the MCU.

##### Assemble #####

* Maxim DS3231M - SOIC-8: **U6**
* Ceramic Capacitor 0.1 uF 6 V - SMD 0603: **C21 and C22**
* Film Resistor 100 kΩ - SMD0603: **R28 and R29**
* Film Resistor 4.7 kΩ - SMD0603: **R30 and R31**
* PCB Headers - 2.54 mm: **P6** *(optional)*

#### Coin Cell — BAT1 ####
For ultra-low power applications, where size and weight matter, a small CR2032 coin cell may be the best option. In this case, an SMD battery retainer can be soldered to the board. The compatible parts are:

* BK-912 — http://www.batteryholders.com/part.php?pn=BK-912
* BAT-HLD-001 — https://www.linxtechnologies.com/en/products/connectors/battery-holders

The coin cell battery terminals are attached directly to the step-up switching regulator, the same way the PicoBlade (CN2) is. The coin cell retainer shares the same PCB space as the 1117 LDO, so having both installed on the same board is not possible.

Before soldering the battery holder, make sure both round pads identified as "BAT1" are covered with a thin layer of solder. This is necessary to raise the contact pads, as well to prevent the battery terminal touching any other footprints.

Bear in mind that a CR2032 can supply 3 V for most of its life but the continuous current delivery is very limited. Simple coding strategies to minimize high peak current draw, like not blinking the LED at the same time as the RFM69 module is being used, will significantly extend the battery's life.

### Modifying Board Configuration ###
To give extra flexibility, some of the MCU pins on the Whisper Node are connected/disconnected through solder jumpers. In a similar way, some components, like buttons and LEDs, can be easily and safely disconnected to free GPIOs according to the user's needs.

#### Via Solder Jumpers ####
Solder jumpers are specially designed cooper pads which can be easily cut or bridged using solder to change the physical configuration of the board. All solder jumpers are located at the back on the board. The table below describes all available jumpers:

|Jumper|Default State|Purpose                   |Note                                                                                  |
|------|-------------|--------------------------|--------------------------------------------------------------------------------------|
|JP1   |Closed       |Battery Voltage           |Connects the battery resistor divider to the MCU A6 Pin                               |
|JP2   |Closed       |VIN Voltage               |Connects the VIN resistor divider to the MCU A7 Pin                                   |
|JP3   |Closed       |Battery Voltage MOSFET    |Controls the MOSFET (via A0) that allows the battery voltage resistor to read voltage*|
|JP4   |Closed       |Powers the RTC from 3V3_R2|Caution when using JP4 and JP5 at same time                                           |
|JP5   |Open         |Powers the RTC from MCU A1|Can be used to power the RTC VIN from one of the MCU pin to enable all RTC features   |
|JP6   |Open         |RTC Int to MCU D3 (Int1)  |Can be used to connect the RTC interrupt pin to the MCU Int1 to use alarms       |
|JP7   |Closed       |RFM69 Reset PIN to MCU D7 |Used to hardware-reset the RFM69 module and bring it to a known state                 |

**This exists to minimize current leakage through the voltage divider when it is not being read.*

#### Via Resistors ####
Because of the density of the board, not all connections can be routed through solder jumpers. But additional modifications can be done to re-purpose MCU pins without causing any damage to the board. The table below shows individual resistors that are safe to remove to disable components, such as an LED or button.

|Resistor/Capacitor|Value       |Purpose             |Note                                                        |
|------------------|------------|--------------------|------------------------------------------------------------|
|R1                |220R        |LED 1 - Blue        |Safe to remove resistor to disable LED and free the MCU pin |
|R2                |220R        |LED 2 - Yellow      |Safe to remove resistor to disable LED and free the MCU pin |
|R6 + C8           |10K + 0.1 uF|BT 1                |Disables button. Removal of the capacitor is optional       |
|R8 + C9           |10K + 0.1 uF|BT 2                |Disables button. Removal of the capacitor is optional       |

**Note that most of the components are size 0402, so a good pair of fine tweezers, a reliable soldering iron, and special care must be used when performing any modification to the board.*

### Schematics ###
Most of the Talk² Whisper Node schematics are available at the "Documentation" folder in this repository: https://bitbucket.org/talk2/whisper-node-avr/src/master/Documentation.

## Power ##
The Talk² Whisper Node can be powered by different sources or even by combining multiple ones. The board has been specially designed to run on batteries, specifically alkaline cells. For this reason, the board comes with two voltage regulators: a Microchip MCP16251 step-up switching regulator and a Diodes Dual LDO AP7332.

The board offers two 3.3 V rails:

* 3V3_R1 is the main rail and is used to power all board components (MCU, radio, flash, etc)
* 3V3_R2 is the auxiliary rail and is only active when the board is powered from the LDO (VIN)

### Battery: VBAT ###
The Battery input is directly connected to the MCP16251 step-up regulator. To be as efficient as possible, there is no diode or protection on this line, so do not reverse the polarity. The VBAT can be powered via the Molex PicoBlade connector or from any PCB VBAT pad (check the board pinouts for details).

Although the board has been tested and is able to run down to 0.75 V, the recommended/start voltage must be between 0.9 V and 3.3 V.

*Note: 3R3V2 is not available when the board is powered by VBAT only.

### Power Supply: VIN ###
The VIN is connected to the LDO AP7332 thorough a Schottky diode. The supplied voltage must be between 3.7 V and 6 V. The VIN can be powered via the Micro-USB connector or from any PCB VIN Pad (check the board pinouts for details).

### By-passing the Regulators ###
An alternative way to power the board is by supplying 3.3 V directly to one of the 3V3_R1 pins. Note that by doing this, you will be by-passing any kind of built-in regulation, so make sure the power source is stable or it might cause permanent damage to the board.

### Power Backup ###
It is important to highlight that VIN will take precedence over VBAT. In other words, once a power supply is connected to VIN, it will disable the step-up regulator by pulling its enable pin low. In this configuration, battery consumption will be as low as 3 uA.

In case the power supply is disconnected, or it stops working, the step-up regulator will be re-enabled and will start feeding 3.3 V to the 3V3_R1.

### Voltage Monitor ###
The Talk² Whisper Node comes with two resistor dividers configured as R Top: 562K and R Bot: 100K. This will result in 0.151 V output for every 1 V input. One resistor divider is attached to the VIN, with the output to A7, and the other is attached to the VBAT through a P+N MOSFET, with the output to A6.

Both voltage dividers can be used to monitor the input voltage levels via the MCU ADC and used to make the application behave accordingly to the available power sources. To save power, the VBAT voltage divider it is not always connected. To read the VBAT voltage divider, first bring the VBAT_Voltage_MOSFET pin high (check the board pinouts for details).

Using the internal MCU analog reference of 1.1 V, it is possible to read up to 7.282 V on any of the voltage dividers. The MCU comes with a 10-bit resolution ADC, in other words, it can have a value from 0 to 1023. In a practical example, if the ADC is reading 211, it means the voltage before the voltage divider is around 1.5 V:

```
Voltage = (MAX_Voltage * ADC_Reading) / 1024
Voltage = (7.282 * 211) / 1024
Voltage = 1.5
```
### Board Power Consumption ###
The board consumption was measured at the battery input (step-up regulator), but instead of a battery, an adjustable bench power supply was used to simulate multiple voltages.

**Test Modes**

* **All Sleeping:** MCU in power-down sleeping mode, BOD and ADC disabled. SPI flash and RFM69 radio module in sleep modes;
* **All Sleeping + RTC:** Same as above plus optional RTC chip soldered on the board and running in time-keeping mode;
* **MCU Running:** MCU in a "delay()" loop with all peripherals turned on. SPI flash and RFM69 radio module in sleep modes.

|Input Voltage|Mode                                  |Current |
|-------------|--------------------------------------|--------|
|3 V          |All Sleeping                          |3.7 µA  |
|3 V          |All Sleeping + RTC                    |9.6 µA  |
|3 V          |MCU Running, Flash and Radio Sleeping |7.8 mA  |
|2.5 V        |All Sleeping                          |4.3 µA  |
|2.5 V        |All Sleeping + RTC                    |11.8 µA |
|2.5 V        |MCU Running, Flash and Radio Sleeping |9.8 mA  |
|1.5 V        |All Sleeping                          |5.4 µA  |
|1.5 V        |All Sleeping + RTC                    |15.5 µA |
|1.5 V        |MCU Running, Flash and Radio Sleeping |17.6 mA |
|1 V          |All Sleeping                          |6.3 µA  |
|1 V          |All Sleeping + RTC                    |18.2 µA |
|1 V          |MCU Running, Flash and Radio Sleeping |27.8 mA |
|0.8 V        |All Sleeping                          |8.1 µA  |
|0.8 V        |All Sleeping + RTC                    |25.6 µA |
|0.8 V        |MCU Running, Flash and Radio Sleeping |36.2 mA |

*Note that the figures may have some variation (+- 2 µA), which can be caused by factors such as temperature, power supply stability, etc. Make sure that the voltage is measured as close as possible to the board.*

### RFM69 Power Consumption ###
The radio module is the component which consumes the most power, especially during transmission. To reduce power usage, a few methods can be implemented:

* Reduce TX power to minimum required;
* Reduce the message size, so less time is spent sending the message;
* Use highest TX speed possible;
* Use "sleep mode" as much as possible.

Below you can find a few oscilloscope captures showing the power consumption using different TX output power. The sent message was 20 bytes long, with 8 bytes of payload and 12 bytes for headers and CRC added by the module. Only the first capture contains all labels and details but the parameters for all captures are the same.

![RFM69 power consumption trace for 13 dBm, 10 dBm, and 7 dBm transmit levels](https://bitbucket.org/repo/drxzG7/images/2609502644-RFM69_13dBm_10dBm_7dBm.png)
![RFM69 power consumption trace for 13 dBm, 4 dBm, and 0 dBm transmit levels](https://bitbucket.org/repo/drxzG7/images/43709075-RFM69_13dBm_4dBm_0dBm.png)
![RFM69 power consumption trace for 13 dBm, -3 dBm, and -7 dBm transmit levels](https://bitbucket.org/repo/drxzG7/images/1618168630-RFM69_13dBm_-3dBm_-7dBm.png)
![RFM69 power consumption trace for 13 dBm, -13 dBm, and -18 dBm transmit levels](https://bitbucket.org/repo/drxzG7/images/3919269816-RFM69_13dBm_-13dBm_-18dBm.png)

*All measurements were taken at the 3V3_R1, just after the regulator output.
The scale is 100 mV = 10 mA, for example: the 13 dBm peak is located at the top of the 6th division, which is read as 600 mV and is equal to 60 mA.*

### Regulator Efficiency ###
Below are some test results from the power supply unit only. The tests were performed on a Talk² Whisper Node assembled only with the power supply components. The current was measured at the input with a no-load and 100 Ω resistive load configuration.

#### Step-up ####
|Input Voltage|Output Voltage|Load*   |Current |Efficiency|
|-------------|--------------|--------|--------|----------|
|3 V          |n/a           |shutdown|3.9 µA  |n/a       |
|3 V          |3.27 V        |no-load |3.2 µA  |n/a       |
|3 V          |3.26 V        |100 Ω   |37.1 mA |97.8 %    |
|2.5 V        |n/a           |shutdown|3.3 µA  |n/a       |
|2.5 V        |3.27 V        |no-load |3.6 µA  |n/a       |
|2.5 V        |3.26 V        |100 Ω   |45.9 mA |94.9 %    |
|1.5 V        |n/a           |shutdown|1.9 µA  |n/a       |
|1.5 V        |3.27 V        |no-load |4.6 µA  |n/a       |
|1.5 V        |3.25 V        |100 Ω   |80 mA   |90.7 %    |
|1 V          |n/a           |shutdown|1.4 µA  |n/a       |
|1 V          |3.27 V        |no-load |5.7 µA  |n/a       |
|1 V          |3.25 V        |100 Ω   |140 mA  |77.7 %    |
|0.8 V        |n/a           |shutdown|1.2 µA  |n/a       |
|0.8 V        |3.27 V        |no-load |5.7 µA  |n/a       |
|0.8 V        |3.25 V        |100 Ω   |180 mA  |75.6 %    |

**The shutdown load represents the current consumption when the board is powered by the LDO regulator, which places the step-up in shutdown mode.*

#### LDO and 1117 ####
|Input Voltage|Output Voltage|Load    |Current |Regulator    |
|-------------|--------------|--------|--------|-------------|
|5 V          |3.28 V        |no-load |62.0 µA |Built-in LDO |
|5 V          |3.28 V        |100 Ω   |33.5 mA |Built-in LDO |
|7.2 V        |3.28 V        |no-load |5.8 mA  |Optional 1117|
|7.2 V        |3.28 V        |100 Ω   |38.1 mA |Optional 1117|
|9 V          |3.28 V        |no-load |5.8 mA  |Optional 1117|
|9 V          |3.28 V        |100 Ω   |38.0 mA |Optional 1117|
|12 V         |3.28 V        |no-load |5.8 mA  |Optional 1117|
|12 V         |3.28 V        |100 Ω   |36.8 mA |Optional 1117|

All tests where performed at room temperature, around 25 °C. The power source used was a Tenma 72-10480 and measurement of the step-up regulator was done through a µCurrent (https://www.eevblog.com/projects/ucurrent/). Also, all connections were kept as short as possible to reduce any voltage-drop across the wires.

# Software #
This section describes a bit about the standard/factory firmware as well the MCU Fuses and Bootloader configuration.

## Arduino IDE Boards ##
If you are using the Arduino IDE, you will need a "Board Configuration" to upload code to the Whisper Node. This can be done by adding the following "Additional Boards Manager URL" to your Arduino IDE preferences: http://talk2arduino.wisen.com.au/master/package_talk2.wisen.com_index.json

For additional information, please refer to the https://bitbucket.org/talk2/arduino-ide-boards repository.

## Library ##
The Talk² Arduino Library can be installed using the "Library Manager" on the Arduino IDE. For details and access to the library source code, please refer to the https://bitbucket.org/talk2/talk2-library repository.

## MCU Fuses ##
By default the Whisper Node AVR is shipped with the following fuses configured:

|Fuse     |Value |
|---------|------|
|High     |0xDA  |
|Low      |0xEF  |
|Extended |0x05  |

This sets the MCU to:

* use the external crystal and clock divider = 1 (resulting in 16 MHz)
* reserve 1024 pages for the bootloader (2048 bytes)
* set the BOD Level 1 (2.7 V)

## Bootloader ##
The Talk² Whisper Nodes is pre-programmed with the Talk² Boot, a modified version of the Optiboot, capable of programming the MCU from an external SPI flash memory. For all features and documentation, please refer to the repository https://bitbucket.org/talk2/talk2boot.

**Default Sketch Upload Speed:** 115200 b/s

## Factory Firmware ##
Apart from the bootloader, the Talk² Whisper Node is also programmed with a factory firmware (or sketch). This firmware is used to test each board during the final phase of the manufacturing process but also provides some additional functionality, such as support for "over-the-air" updates from the factory.

The same firmware is present on the MCU flash as well in a **write-protected** area of the external SPI flash, specifically, from the memory address 0x0007000 to 0x0007FFFF. This memory address is the same one used by the Talk² Bootloader in case a "Factory Reset Flow" is triggered.

It is safe to replace the factory firmware with your own code, including the firmware image saved on the SPI flash. This firmware is just installed by default for testing proposes and to enable remote programming out-of-the-box.

**Note that if you wish to erase the whole SPI flash memory, you need to first disable the write-protection.*

### Factory Reset Flow ###
To trigger the "Factory Reset Flow" on the Talk² Whisper Node board, you must hold down BT1 (button 1) while powering on the board. Wait until LD1 (blue LED) starts to blink quickly and release the button while LD1 is still blinking. LD2 (yellow LED) will blink quickly a few times indicating the MCU is being programmed, followed by a reset.

For all bootloader features and documentation, please refer to the repository https://bitbucket.org/talk2/talk2boot.

### Factory Firmware Configuration ###
One way to access the firmware configuration is to connect it to a serial port at the speed of 115200 b/s:

![Screen capture showing Whisper Node Factory Firmware configuration](https://bitbucket.org/repo/drxzG7/images/1675741079-WhisperNode_FactoryFirmware_01.png)

Another way to find the configuration is be access the "Factory Firmware" example code, part of the "T2WhisperNode" library.

#### Configuration Table ####

|Parameter |433 MHz Standard |433 MHz High-Power |868 MHz Standard |868 MHz High-Power |915 MHz Standard |915 MHz High-Power |
|----------|-----------------|-------------------|-----------------|-------------------|-----------------|-------------------|
|Frequency |434 MHz          |434 MHz            |868 MHz          |868 MHz            |916 MHz          |916 MHz            |
|TX Power  |13 dbM           |14 dbM             |13 dbM           |14 dbM             |13 dbM           |14 dbM             |

### Remote Programming ###
TBD: How to use the Factory Firmware to remote program the Whisper Node.